const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload();

// SRC file
const styleSRC = 'src/scss/style.scss';
const styleDIST = './dist/css/';

const jsSRC = 'script.js';
const jsFolder = 'src/js/';
const jsDIST = './dist/js/';
const jsFILES = [jsSRC]

// watch 
const styleWatch = 'src/scss/**/*.scss';
const jsWatch = 'src/js/**/*.js';
const htmpWatch = '**/*.html';

function browserSynch(done) {
    // Localhost
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

    // Server
    // browserSync.init({
    //     open: false,
    //     injectChanges: true,
    //     proxy: 'http://gulp.dev',
    //     https: {
    //         key: '',
    //         cert: ''
    //     }
    // });
    done();
}

// Functions
function testGulp(done) {
    console.log('gulp is working');
    done();
}

function styles(done) {
    gulp.src(styleSRC)
        .pipe( sourcemaps.init() )
        .pipe( sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }) )
        .on( 'error', console.error.bind(console) )
        .pipe( autoprefixer({
            cascade: false
        }) )
        .pipe( rename({ suffix: '.min' }) )
        .pipe( sourcemaps.write('./') )
        .pipe( gulp.dest(styleDIST) )
        .pipe( browserSync.stream() );
    done();
}

function js(done) {
    jsFILES.map(function(file) {
        return browserify(jsFolder + file)
            .transform(babelify, { "presets": ["@babel/preset-env"] })
            .bundle()
            .pipe( source(file) )
            .pipe( rename({
                extname: '.min.js'
            }) )
            .pipe( buffer() )
            .pipe( sourcemaps.init({
                loadMaps: true
            }) )
            .pipe( uglify() )
            .pipe( sourcemaps.write('./') )
            .pipe(gulp.dest(jsDIST))
            .pipe( browserSync.stream() );
    });
    done();
}

// Task name
gulp.task(testGulp);
gulp.task(styles);
gulp.task(js);
gulp.task(browserSynch);

function watch() {
    gulp.watch(styleWatch, styles);
    gulp.watch(jsWatch, js).on('change', browserSync.reload);
    gulp.watch(htmpWatch).on('change', browserSync.reload);
}

const build = gulp.parallel(
    styles,
    js,
    browserSynch,
    gulp.series(watch)
);

exports.default = build;